# ROS开发环境配置教程

文档撰写：廖伦稼
##  本次教程的软件版本
ubuntu: 20.04  
ROS: noetic  
CLion: 2021.1.1  
catkin_tools   (note:本教程使用catkin_tools 而不是 catkin_make)

##  参考资料
CLion的下载以及基本配置可参考[Install CLion](https://www.jetbrains.com/help/clion/installation-guide.html)  

catkin_tools的下载以及基本配置可参考[install catkin_tools](https://catkin-tools.readthedocs.io/en/latest/installing.html)  

catkin下CMakeLists.txt编写教程： [catkin/CMakeLists.txt](http://wiki.ros.org/catkin/CMakeLists.txt)
#  开发流程
## 1、创建工作空间

```bash
source /opt/ros/noetic/setup.bash          # Source ROS noetic to use Catkin
mkdir -p ~/rm_ws/src            # Make a new workspace and source space
```
## 2、初始化工作空间

```bash
cd rm_ws/src/
catkin_init_workspace    # 创建顶层CMakeLists.txt文件，CLion利用此文件打开项目
```

## 3、编译生成完整工作区

```bash
cd ~/rm_ws
catkin build
```
## 4、 查看一下当前目录结构
这时可以看到生成了几个文件夹 `build` 、 `devel` 、 `logs`
![pic](PIC/01.png)

### 将路径添加到`.bashrc`，下次打开terminal就自动source rm_ws
```bash
echo "source ~/rm_ws/devel/setup.bash" >> ~/.bashrc 
```
**关掉这个terminal**

## 4. 打开新的terminal，并在里面启动 `clion.sh`

```bash
clion.sh   #这里启动的路径可能不一样
```

打开CLion后选择CMakeLists.txt打开项目
![pic](PIC/02.png)

![pic](PIC/03.png)

## 5. 配置CLion
上一步打开CLion后应该会自动跳出项目向导，需要配置一下，如果没有跳出该画面，则可以手动打开 File | Settings | Build, Execution, Deployment | CMake

这里的CMake选项和构建目录要改一下，改成catkin能识别的，不然会指向另一个自动生成的构建目录。
```bash
-DCATKIN_DEVEL_PREFIX=../devel
../build
```
![pic](PIC/04.png)

不出意外的话，打开后可以看到如下界面和工程结构
![pic](PIC/05.png)


## 6. 创建两个 ROS package

打开新的terminal，创建两个rospackage

```bash
cd rm_ws/src/
catkin create pkg pkg_a
catkin create pkg pkg_b
```
![pic](PIC/06.png)


## 7. 在 pkg_a 创建 talker

### 7.1 在 pkg_a 创建 src/talker.cpp
```cpp
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <sstream>

int main(int argc, char **argv) {
  ros::init(argc, argv, "talker"); //talker is node name
  ros::NodeHandle n;

  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok()) {
    std_msgs::String msg;
    std::stringstream ss;
    ss << "hello world " << count;
    msg.data = ss.str();

    ROS_INFO("%s", msg.data.c_str());

    chatter_pub.publish(msg);
    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

  return 0;
}
```

### 7.2 修改pkg_a 的package.xml
```xml
<?xml version="1.0"?>
<package format="2">
  <name>pkg_a</name>
  <version>0.0.0</version>
  <description>The pkg_a package</description>

  <maintainer email="liao@todo.todo">liao</maintainer>

  <license>TODO</license>
  
  <buildtool_depend>catkin</buildtool_depend>

  <depend>roscpp</depend>

  <!-- The export tag contains other, unspecified, tags -->
  <export>
    <!-- Other tools can request additional information be placed here -->
  </export>
</package>
```

### 7.3 修改pkg_a 的CMakeList.txt
```cmake
cmake_minimum_required(VERSION 3.0.2)
project(pkg_a)

## Use C++14
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(catkin REQUIRED COMPONENTS
        roscpp)

catkin_package(
        CATKIN_DEPENDS
        roscpp
)
include_directories(
        ${catkin_INCLUDE_DIRS}
)

add_executable(talker src/talker.cpp)
target_link_libraries(talker ${catkin_LIBRARIES})
```

## 8. 在 pkg_b 创建 listener
### 8.1 在 pkg_b 创建 src/listener.cpp
```cpp
#include <ros/ros.h>
#include <std_msgs/String.h>

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);

  ros::spin();
  return 0;
}
```

### 8.2 修改pkg_b 的package.xml
```xml
<?xml version="1.0"?>
<package format="2">
  <name>pkg_b</name>
  <version>0.0.0</version>
  <description>The pkg_b package</description>

  <maintainer email="liao@todo.todo">liao</maintainer>

  <license>TODO</license>
  <buildtool_depend>catkin</buildtool_depend>

  <depend>roscpp</depend>

  <!-- The export tag contains other, unspecified, tags -->
  <export>
    <!-- Other tools can request additional information be placed here -->

  </export>
</package>

```

### 8.3 修改pkg_b 的CMakeList.txt
```cmake
cmake_minimum_required(VERSION 3.0.2)
project(pkg_b)

## Use C++14
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(catkin REQUIRED COMPONENTS
        roscpp)

catkin_package(
        CATKIN_DEPENDS
        roscpp
)
include_directories(
        ${catkin_INCLUDE_DIRS}
)

add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})
```


## 9. 编译rm_ws工作区的所有ros package

```bash
cd ~/rm_ws
catkin build
```
![pic](./PIC/08.png)
##  10.  运行talker 和 listener
打开3个命令行窗口

命令行窗口一运行roscore

```bash
roscore
```
命令行窗口二运行listener

```bash
rosrun pkg_b listener
```
命令行窗口三运行talker

```bash
rosrun pkg_a talker
```
不出意外的话，可以看到两个结点运行成功
![在这里插入图片描述](./PIC/07.png)